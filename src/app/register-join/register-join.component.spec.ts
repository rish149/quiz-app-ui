import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterJoinComponent } from './register-join.component';

describe('RegisterJoinComponent', () => {
  let component: RegisterJoinComponent;
  let fixture: ComponentFixture<RegisterJoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterJoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterJoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
